//
//  UIViewController+AlertShowing.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import UIKit

extension UIViewController{
    func showErrorAlert(error:CommonError) {
        let alert = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
        alert.addAction((UIAlertAction(title: "Aceptar", style: .default, handler: nil)))
        present(alert, animated: true, completion: nil)
    }
}
