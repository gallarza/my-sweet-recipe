//
//  FileManager+ReadJSON.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

extension FileManager {
    
    static func readJson(forResource fileName: String ) -> Data? {
        
        let bundle = Bundle(for: MealListDummyService.self)
        if let path = bundle.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                // handle error
            }
        }
        
        return nil
    }
}
