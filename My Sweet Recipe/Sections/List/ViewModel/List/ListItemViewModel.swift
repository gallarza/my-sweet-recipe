//
//  ListItemViewModel.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

struct ListItemViewModel {
    
    weak var dataSource : GenericDataSource<MealListItem>?
    weak var service: MealListServiceProtocol?
    
    var onErrorHandling : ((ResponseError?) -> Void)?
    
    init(dataSource : GenericDataSource<MealListItem>?) {
        self.dataSource = dataSource
        self.service = WebServicesManager.isDummy ? MealListDummyService.shared : MealListService.shared
    }
    
    func fetchRecipes( withString string:String ) {
        
        guard let service = service else {
            onErrorHandling?(ResponseError.custom(string: "Missing service"))
            return
        }
        
        service.fetchListItemParser( searching: string ) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let converter) :
                    self.dataSource?.data.value = converter.meals
                case .failure(let error) :
                    self.onErrorHandling?(error)
                }
            }
        }
    }
}

