//
//  RandomBannerDataSource.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation
import UIKit

class BannerDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}

class RandomBannerDataSource : BannerDataSource<MealListItem>, UpdateBannerProtocol {
    func updateBanner() -> String? {
        
        guard self.data.value.count > 0 else { return nil }
        
        return self.data.value[0].strMealThumb
    }
}
