//
//  RandomBannerViewModel.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

struct RandomBannerViewModel {
    
    weak var dataSource : BannerDataSource<MealListItem>?
    weak var service: RamdomBannerServiceProtocol?
    
    var onErrorHandling : ((ResponseError?) -> Void)?
    
    init(dataSource : BannerDataSource<MealListItem>?) {
        self.dataSource = dataSource
        self.service = WebServicesManager.isDummy ? RandomBannerDummyService.shared : RandomBannerService.shared
    }
    
    func fetchRandomBanner() {
        
        guard let service = service else {
            onErrorHandling?(ResponseError.custom(string: "Missing service"))
            return
        }
        
        service.fetchRandomBannerParser { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let converter) :
                    self.dataSource?.data.value = converter.meals
                case .failure(let error) :
                    self.onErrorHandling?(error)
                }
            }
        }
    }
}
