//
//  ListCellViewCell.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//


import UIKit

class ListCellViewCell: UICollectionViewCell {

    @IBOutlet weak var mealLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var thumbImage: UIImageView!
    
    var meal : MealListItem? {
        didSet {
            
            guard let meal = meal else {
                return
            }
            
            mealLabel?.text = meal.strMeal
            categoryLabel?.text = meal.strCategory
            thumbImage?.loadImageUsingCacheWithURLString(meal.strMealThumb, placeHolder: UIImage(named:"bannerPlaceholder") )
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
