//
//  ListViewController.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import UIKit

protocol UpdateBannerProtocol {
    func updateBanner() -> String?
}

class ListViewController: UIViewController {
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var bannerView : UIImageView!
    
    var selectedMeal: MealListItem?
    
    let dataSource = ListDataSource()
    let bannerDataSource = RandomBannerDataSource()
    
    lazy var viewModel : ListItemViewModel = {
        let viewModel = ListItemViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    lazy var bannerViewModel : RandomBannerViewModel = {
        let viewModel = RandomBannerViewModel(dataSource: bannerDataSource)
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = dataSource
        self.dataSource.data.addAndNotify(observer: self) { [weak self] _ in
            self?.collectionView.reloadData()
        }
        
        self.bannerDataSource.data.addAndNotify(observer: self) { [weak self] _ in
            
            guard let url = self?.bannerDataSource.updateBanner() else {
                self?.bannerView.image = UIImage(named: "bannerPlaceholder")
                return
            }
            
            self?.bannerView.loadImageUsingCacheWithURLString( url, placeHolder: UIImage(named: "bannerPlaceholder"))
        }
        
        self.viewModel.onErrorHandling = { [weak self] error in
            // display error ?
            let controller = UIAlertController(title: "An error occured", message: "Oops, something went wrong!", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            self?.present(controller, animated: true, completion: nil)
        }
        
        self.viewModel.fetchRecipes(withString: "")
        
        Timer.scheduledTimer(withTimeInterval: 30.0, repeats: true) { [weak self] timer in
            self?.bannerViewModel.fetchRandomBanner()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "showDetailSegue",
            let vc = segue.destination as? MealDetailViewController else { return }
        vc.meal = selectedMeal
    }
    
}

extension ListViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard dataSource.data.value.count > indexPath.row else { return }
        
        selectedMeal = dataSource.data.value[indexPath.row]
        
        performSegue(withIdentifier: "showDetailSegue", sender: nil)
    }
}

extension ListViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
          let textRange = Range(range, in: text) {
          let updatedText = text.replacingCharacters(in: textRange,
                                                      with: string)
          self.viewModel.fetchRecipes(withString: updatedText)
       }
       return true
    }
    
}
