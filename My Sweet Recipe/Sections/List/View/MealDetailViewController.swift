//
//  MealDetailViewController.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation
import UIKit

class MealDetailViewController: UIViewController {

    @IBOutlet weak var video : UIView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var instructionsLabel : UITextView!
    
    var meal : MealListItem? {
        didSet {
            guard let meal = meal else {
                return
            }
            
            titleLabel?.text = meal.strMeal
            instructionsLabel?.text = meal.strInstructions
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel?.text = meal?.strMeal
        instructionsLabel?.text = meal?.strInstructions
    }
    
}
