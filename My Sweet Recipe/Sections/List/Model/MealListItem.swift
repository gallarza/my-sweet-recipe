//
//  MealListItem.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

struct MealListItem {
    let strMealThumb : String
    let strMeal : String
    let strInstructions : String
    let strCategory : String
}
