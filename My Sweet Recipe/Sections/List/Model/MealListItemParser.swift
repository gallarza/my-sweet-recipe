//
//  MealListItemParser.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

struct MealListItemParser {
    let meals : [MealListItem]
}

extension MealListItemParser : Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Result<MealListItemParser, ResponseError> {
        if let meals = dictionary["meals"] as? [[String: Any]] {
            
            let finalMeals : [MealListItem] = meals.map{ meal in
                return MealListItem(strMealThumb: meal["strMealThumb"] as? String ?? "" , strMeal: meal["strMeal"] as? String ?? "", strInstructions: meal["strInstructions"] as? String ?? "", strCategory: meal["strCategory"] as? String ?? "")
            }
            
            let conversion = MealListItemParser(meals: finalMeals)
            
            return Result.success(conversion)
        } else {
            return Result.failure(ResponseError.parser(string: "Unable to parse conversion rate"))
        }
    }
}
