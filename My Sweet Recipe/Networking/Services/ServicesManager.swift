//
//  ServicesManager.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

enum services {
    case search
    case random
    case detail
}

struct WebServicesManager {

    static func getPlist() -> [String:Any]? {
        guard let path = Bundle.main.path(forResource: "WebServices", ofType: "plist") else {return nil}
        let url = URL(fileURLWithPath: path)
        let data = try! Data(contentsOf: url)
        let plist = try! PropertyListSerialization.propertyList(from:data, format: nil) as! [String:Any]

        return plist
    }
    
    static let isDummy:Bool = {
        guard   let dict = getPlist(),
                let isDummy = dict["isDummy"] as? Bool else { return false }
        return isDummy
    }()
    
    static func getService(_ service:services) -> String? {
        
        guard   let dict = getPlist(),
                let base = dict["baseURL"] as? String,
                let endpoints = dict["endpoints"] as? [String:String]  else { return nil }
        
        switch service {
        case .search:
            return base + (endpoints["List"] ?? "")
        case .random:
            return base + (endpoints["Random"] ?? "")
        case .detail:
            return base + (endpoints["Detail"] ?? "")
        }
        
    }
    
}
