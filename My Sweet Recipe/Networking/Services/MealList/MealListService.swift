//
//  MealListService.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

protocol MealListServiceProtocol : class {
    func fetchListItemParser( searching strSearch:String, _ completion: @escaping ((Response<MealListItemParser, ResponseError>) -> Void))
}

final class MealListService : RequestHandler, MealListServiceProtocol {
    
    static let shared = MealListService()
    
    var task : URLSessionTask?
    
    func fetchListItemParser( searching strSearch:String, _ completion: @escaping ((Response<MealListItemParser, ResponseError>) -> Void)) {
        self.cancelCurrentRequest()
        
        guard var endpoint = WebServicesManager.getService(.search) else { return }
        
        endpoint.append("?s=\(strSearch)")
        
        task = RequestService().loadData(urlString: endpoint, completion: self.networkResult(completion: completion))
    }
    
    func cancelCurrentRequest() {
        
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}
