//
//  RamdomBannerService.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

protocol RamdomBannerServiceProtocol : class {
    func fetchRandomBannerParser( _ completion: @escaping ((Response<MealListItemParser, ResponseError>) -> Void))
}

final class RandomBannerService : RequestHandler, RamdomBannerServiceProtocol {
    
    static let shared = RandomBannerService()
    
    var endpoint = WebServicesManager.getService(.random)
    var task : URLSessionTask?
    
    func fetchRandomBannerParser( _ completion: @escaping ((Response<MealListItemParser, ResponseError>) -> Void)) {
        self.cancelCurrentRequest()
        
        guard endpoint != nil else {return}
        
        task = RequestService().loadData(urlString: endpoint!, completion: self.networkResult(completion: completion))
    }
    
    func cancelCurrentRequest() {
        
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}

