//
//  RandomBannerDummyService.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

final class RandomBannerDummyService : RamdomBannerServiceProtocol {
    
    static let shared = RandomBannerDummyService()
    
    func fetchRandomBannerParser( _ completion: @escaping ((Response<MealListItemParser, ResponseError>) -> Void)) {
        
        // giving a sample json file
        guard let data = FileManager.readJson(forResource: "RandomBannerSample") else {
            completion(Response.failure(ResponseError.custom(string: "No file or data")))
            return
        }
        
        ParserHelper.parse(data: data, completion: completion)
    }
}
