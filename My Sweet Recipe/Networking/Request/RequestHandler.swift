//
//  RequestHandler.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

class RequestHandler {
    
    let reachability = Reachability()!
    
    func networkResult<T: Parceable>(completion: @escaping ((Response<[T], ResponseError>) -> Void)) ->
        ((Response<Data, ResponseError>) -> Void) {
            
            return { dataResult in
                
                DispatchQueue.global(qos: .background).async(execute: {
                    switch dataResult {
                    case .success(let data) :
                        ParserHelper.parse(data: data, completion: completion)
                        break
                    case .failure(let error) :
                        print("Network error \(error)")
                        completion(.failure(.conectivity(string: "Network error " + error.localizedDescription)))
                        break
                    }
                })
                
            }
    }
    
    func networkResult<T: Parceable>(completion: @escaping ((Response<T, ResponseError>) -> Void)) ->
        ((Response<Data, ResponseError>) -> Void) {
            
            return { dataResult in
                
                DispatchQueue.global(qos: .background).async(execute: {
                    switch dataResult {
                    case .success(let data) :
                        ParserHelper.parse(data: data, completion: completion)
                        break
                    case .failure(let error) :
                        print("Network error \(error)")
                        completion(.failure(.conectivity(string: "Network error " + error.localizedDescription)))
                        break
                    }
                })
                
            }
    }
}
