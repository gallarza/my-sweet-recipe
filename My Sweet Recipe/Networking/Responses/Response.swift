//
//  Response.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

enum Response<T, E: Error> {
    case success(T)
    case failure(E)
}

enum ResponseError: Error {
    case conectivity(string: String)
    case parser(string: String)
    case custom(string: String)
}
