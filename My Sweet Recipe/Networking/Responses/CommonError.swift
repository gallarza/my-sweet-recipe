//
//  CommonError.swift
//  My Sweet Recipe
//
//  Created by Juan Gallarza on 10/10/20.
//  Copyright © 2020 Gallarza. All rights reserved.
//

import Foundation

struct CommonError {
    var title:String?
    var message:String?
    
    init(error:ResponseError) {
        
    }
    
    init(err:Error) {
        title = "Error de servicio"
        message = err.localizedDescription
    }
}
